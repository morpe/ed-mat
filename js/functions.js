// NEW selector
jQuery.expr[':'].Contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

// OVERWRITES old selecor
jQuery.expr[':'].contains = function (a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

function makeDelay(ms) {
    var timer = 0;
    return function(callback){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
};

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function ts2date(timestamp) {

    if (timestamp == undefined) return "timestamp undefined";

    uts = Date.parse(timestamp);
    tm = new Date(uts);

    y = tm.getFullYear();
    M = months[tm.getMonth()];
    d = tm.getDate().toString();
    h = tm.getHours().toString();
    p = tm.getMinutes().toString(); //prime
    s = tm.getSeconds().toString();

    if (d.length == 1) d = "0" + d;
    if (h.length == 1) h = "0" + h;
    if (p.length == 1) p = "0" + p;
    if (s.length == 1) s = "0" + s;

    timeSep = "<span style='opacity:0.3;' >:</span>";
    dateTimeSep = " <span style='opacity:0.3;' >@</span> ";

    tms = y + " " + M + " " + d + dateTimeSep + h + timeSep + p + timeSep + s;
    return "<span id='time" + uts + "'  class='ts2date' data-timestamp='" + timestamp + "' data-unix-ts='" + uts + "' >" + tms + "</span>";
}

Materials = {}
Materials.data = materials;
Materials.html = false;
Materials.grade = {
    "Very common": 1,
    "Common": 2,
    "Standard": 3,
    "Rare": 4,
    "Very rare": 5,
}
Materials.Cell = function (arr) {

    if (arr[1]) {
        cell = [];
        cell.push("<div class='mtCell'>");

        cell.push("<div class='mtName'>" + arr[1].Name + "</div>");

        cell.push("<div class='mtPrp'>");
        cell.push("<span class='mtKind'>" + arr[1].Kind + "</span>")
        cell.push("<span class='mtGrp'>" + arr[1].Group + "</span>")
        cell.push("<span class='mtRar " + arr[1].Rarity + "'>" + arr[1].Rarity + "</span>")
        cell.push("</div>");

        cell.push("<div class='mtDetails'>");

        if (arr[1].OriginDetails) {
            cell.push(arr[1].OriginDetails.join(", "));
        } else {
            cell.push("::: unknown locations :::");
        }
        cell.push("</div>");
        cell.push("</div>");

        this.html = cell.join("\n");
    } else {
        console.log(arr);
    }

}
Materials.Get = function (name = false) {
    if (name == false) return false;

    wordRef = {
        "legacyfirmware": "specialisedlegacyfirmware",
        "scanarchives": "unidentifiedscanarchives",
        "shieldpatternanalysis": "aberrantshieldpatternanalysis",
        "disruptedwakeechoes": "atypicaldisruptedwakeechoes",
        "shieldcyclerecordings": "distortedshieldcyclerecordings",
        "shieldsoakanalysis": "inconsistentshieldsoakanalysis",
        "wakesolutions": "strangewakesolutions",
        "shieldfrequencydata": "peculiarshieldfrequencydata",
        "scrambledemissiondata": "exceptionalscrambledemissiondata",
        "fsdtelemetry": "anomalousfsdtelemetry",
        "bulkscandata": "anomalousbulkscandata",
        "adaptiveencryptors": "adaptiveencryptorscapture",
        "encryptionarchives": "atypicalencryptionarchives",
        "encryptionarchives": "atypicalencryptionarchives",
        "consumerfirmware": "modifiedconsumerfirmware",
        "industrialfirmware": "crackedindustrialfirmware",
        "encryptioncodes": "taggedencryptioncodes",
        "securityfirmware": "securityfirmwarepatch",
        "encryptedfiles": "unusualencryptedfiles",
        "symmetrickeys": "opensymmetrickeys",
        "embeddedfirmware": "modifiedembeddedfirmware",
        "securityfirmware": "securityfirmwarepatch",
        "industrialfirmware": "crackedindustrialfirmware",
        "embeddedfirmware": "modifiedembeddedfirmware",
        "industrialfirmware": "crackedindustrialfirmware",
        "embeddedfirmware": "modifiedembeddedfirmware",
        "industrialfirmware": "crackedindustrialfirmware",
        "hyperspacetrajectories": "eccentrichyperspacetrajectories",
        "dataminedwake": "dataminedwakeexceptions",
        "hyperspacetrajectories": "eccentrichyperspacetrajectories",
        "dataminedwake": "dataminedwakeexceptions",
        "hyperspacetrajectories": "eccentrichyperspacetrajectories",
        "embeddedfirmware": "modifiedembeddedfirmware",
        "securityfirmware": "securityfirmwarepatch",
        "encryptioncodes": "taggedencryptioncodes",
        "industrialfirmware": "crackedindustrialfirmware",
        "encryptioncodes": "taggedencryptioncodes",
        "symmetrickeys": "opensymmetrickeys",
        "embeddedfirmware": "modifiedembeddedfirmware",
        "industrialfirmware": "crackedindustrialfirmware",
        "hyperspacetrajectories": "eccentrichyperspacetrajectories",
        "industrialfirmware": "crackedindustrialfirmware",
        "encryptedfiles": "unusualencryptedfiles",
        "industrialfirmware": "crackedindustrialfirmware",
        "symmetrickeys": "opensymmetrickeys",
        "encryptedfiles": "unusualencryptedfiles",
        "encryptioncodes": "taggedencryptioncodes",
        "symmetrickeys": "opensymmetrickeys",
        "encryptedfiles": "unusualencryptedfiles",
    }

    if (wordRef[name]) name = wordRef[name];

    name = name.toLocaleLowerCase().replace(/\s+/g, '');
    out = []
    out.push({
        "searchFor": name
    });

    for (var id in materials) {
        //console.log( materials[id]['FormattedName'] )
        matFN = materials[id]['FormattedName'];
        if (matFN == name) {
            out.push(materials[id])
        }
    }

    if (out.length == 1) {
        for (var id in materials) {
            matFN = materials[id]['FormattedName'];
            if (matFN.indexOf(name) != -1) {
                out.push(materials[id])
            }
        }

        out.push(false);
        //console.log('"' + name + '":"' + out[1].FormattedName + '",');
    }


    this.Cell(out);

    return out;

}

function evls() {
    $(".events thead tr").html("");
    $(".events tbody").html("");
    $(".byMat, .bySys").fadeOut(250);
    $(".events").fadeIn(250)
    

    /* intestazione della tabella */

    tHead = ["timestamp", "StarSystem", "Name", "", "Count", "event", "eventType"];
    tHead = ["timestamp", "StarSystem", "Name", "Count", "event", "eventType"];
    TH = [];

    TH.push("\t<th class='col col-h col-id center-align' >#</th>");
    for (var th in tHead) {
        //console.log(th);
        TH.push("\t<th class='col col-h col-" + tHead[th] + " center-align' >" + tHead[th] + "</th>");
    }
    TH.push("\t<th class='col col-h col-scroll center-align' >|</th>");

    $(".events thead tr").html(TH.join("\n"));

    /* -------------- data ----------------------- */

    allTR = []
    for (var nn in events) {
        singleTD = [];
        evNN = events[nn];
        //console.log(evNN['timestamp']);
        singleTD.push("\t<td class='col col-h col-id center-align' >" + nn + "</td>");
        for (var th in tHead) {
            col = tHead[th];
            TD = evNN[col];
            tdObj = [];

            if (col == "timestamp") TD = ts2date(TD);

            if (col == "Name") {
                nameTD = TD;
                Materials.Get(nameTD);
                singleTD.push("\t<td class='col col-"+th+" col-h col-" + col + "' data-mat-property='' >" + Materials.html + "</td>");
            } else {

                if (TD) {
                    singleTD.push("\t<td class='col col-"+th+" col-h col-" + col + "' data-mat-property='' >" + TD + "</td>");
                    //console.log(TD);
                } else {
                    singleTD.push("\t<td class='col col-"+th+" col-h col-" + col + "' >-</td>");
                }
            }
        }

        oddeven = "odd";
        if (nn % 2 == 0) oddeven = "even";

        allTR.push("<tr id='r" + nn + "' class='" + oddeven + "'>" + singleTD.join("\n") + "</tr>");
    }

    //$(".events tbody").html( allTR.join("\n") );

    var data = allTR;
    var clusterize = new Clusterize({
        rows: data,
        scrollId: 'scrollArea',
        contentId: 'contentArea'
    });

    //console.log(allTR);
}

bySys = { "data":bySys };
bySys.html = [];
bySys.list = [];
bySys.findModel = "material";
bySys.input = "div#finder input";

bySys.Show = function(){
    $(".events, .byMat").fadeOut(250);
    $(".bySys").delay(250).fadeIn(250);

    $(bySys.input).on("keyup",function(){
        val = $(this).val();
        $(bySys.input).delay(250).queue(function(){    
            bySys.Finder(val);
            $( this ).dequeue();
        })
    });


    $("div#options").html(" ");
    $("div#options").append("<span id='find-by-mat' class='find-model' data-val='material'>search Materials</span>");
    $("div#options").append("<span id='find-by-ss' class='find-model' data-val='system'>search Systems</span>");
    $("div#options").append("<span id='result' class='result'></span>");
    $("div#options").append("<span id='looking-for' class='looking-for'></span>");

    $("span.find-model").on("click",function(){
        $("span.find-model").removeClass("set");
        mode = $(this).data("val");
        $(this).addClass("set");
        bySys.findModel = mode;
    });

    $("span#find-by-mat").click();
    
    this.Build();

    $("div.bySys div.sys").each(function(a,b){
        $(b).attr("id","sys"+a);
    });
    
}
bySys.Finder = function(ts){

    console.log("finder",ts.length);
    ts = ts.toLowerCase();

    if( this.findModel == "system" ){
        $.each(this.list,function(a,b){
            if( b.toLowerCase().indexOf( ts ) != -1 ){
                $("div#sys"+a).show();
            }
            else{
                $("div#sys"+a).hide();
            }
        });
    }
    
    if( this.findModel == "material" ){

        _ts_ = ts;
        ts = Sugar.String.removeAll(ts," ");

        matData = Materials.Get(ts)
        css = "";
        looking_for =[]
        looking_for.push("<span>looking for:</span>");
        $.each(matData,function(a,b){
            if( b["Name"] ){
                FormattedName = b['FormattedName'];
                li = "color: #00ceff;";
                css += "li."+FormattedName.toLowerCase()+" span.matN{ "+li+" }\n";
                looking_for.push("<span class='mat-name'>"+FormattedName+"</span>");
            }
        });
       
        $("#dyn-theme").html(css);

        if( looking_for.length < 12 ){
            $("span.looking-for").html(looking_for.join("\n"));
        }
        else{
            $("span.looking-for").html(" ");
        }

        QR = SEARCHJS.matchArray(bySys.SysMat,{mat:ts,_text:true});
        SS = [];
        
        for( var itm in QR ){
            ss = QR[itm]['system'];
            div = $("")
            SS.push( QR[itm]['system'] );
        }
        
        SS = Sugar.Array.unique(SS);
        
        $(this.input).delay(5).queue(function(){
            
            if( ts.length > 0 ){

                $("div.bySys div.sys").hide();

                $.each( SS, function(a,b){
                    ss = Sugar.String.removeAll(b," ");
                    ss = "div.bySys div."+ss;
                    //console.log(a,ss);
                    try{
                        $(ss).show();
                    }
                    catch{
                        console.log("------------>",{"err":b,a});
                    }
                } );
            }
            else{
                $("div.bySys div.sys").show();
                $("div#options span.result").html("");
            }

            $(this).dequeue();
            $("div#options span.result").html("found <span>"+_ts_+"</span> in "+SS.length+" Systems");

        });
    }

    $(this.input).delay(5).queue(function(){
        if( ts.length <= 2){
            $("div#options span#result").hide(250);
            $("div#options span#looking-for").hide(250);
            $(this).dequeue();
        }
        else{
            $("div#options span#result").show(250);
            $("div#options span#looking-for").show(250);
            $(this).dequeue();
        }

    });
    

};
bySys.Build = function(){
    //console.log( this.data );
    for( var ss in this.data ){
        this.list.push(ss);
    }

    bySys.list.sort();

    /* questo serve per fare la ricerca */
    bySys.SysMat = [];
    $.each( this.data,function(a,b){
        system = a;
        $.each(b,function(aa,bb){
            type = bb['type'];
            mat = Object.keys(bb)[1];
            cc = bb[mat];
            bySys.SysMat.push( {system,type,mat,cc} );
        });
    } );


    for( var ss in this.list ){

        //console.log( this.list[ss] )

        ss = this.list[ss];

        ssAsClass = Sugar.String.removeAll(ss," ");

        div = [];

        div.push("<div class='sys "+ssAsClass+"'>"); 

        div.push("<h4>");
        div.push(ss);
        div.push("</h4>");
        div.push("<span class='sysStat'>found {matC} materials of {typeMatC} different type</span>");

        div.push("<div class='bySysList'>");
        div.push("<ul>");

        matC = 0;
        typeMatC = 0;

        $.each(this.data[ss],function(a,b){
            k = Object.keys(b);
            mat = k[1];
            type = k[0];
            matN = b[mat];
            matTP = b[type];
            matObj = Materials.Get(mat);

            liMat = Sugar.String.removeAll(matObj[1].Name ," ");



            div.push("<li class='"+matTP+" "+liMat.toLowerCase()+"' >");
            div.push("<span class='matType'>"+matTP+" </span> ");
            div.push("<span class='matN'> "+matObj[1].Name+":</span> ");
            div.push("<span class='matC'>"+matN+"</span>");
            div.push("</li>");

            matC = matC + parseInt(matN);
            typeMatC++;

        });

        div.push("</ul>");
        div.push("</div>");

        div.push("</div>");

        div = div.join( "\n" );
        div = div.replace("{matC}",matC);
        div = div.replace("{typeMatC}",typeMatC);
        
        this.html.push(div);

    }

    $("div.bySys").html( this.html.join("\n") );

}



byMat = { "data":byMat };
byMat.html = [];
byMat.Show = function(){
    $(".events, .bySys").fadeOut(250);
    $(".byMat").delay(250).fadeIn(250);
    this.Build();
}
byMat.Build = function(){

    for(var mat in this.data ){
        
        div = [];

        div.push("<div class='mat'>");
        
        div.push("<h4>");
        div.push( Materials.Get(mat)[1].Name );
        div.push("<span>[{matC}:{ssC}]</span>");
        div.push("</h4>");
        
        div.push("<div>");
        div.push("<ul>");

        ssC = 0;
        matC = 0;

        $.each(this.data[mat], function(a,b){
            ss = Object.keys(b);
            nn = b[ss];
            div.push("<li>");
            div.push("<span class='ss'>"+ss+":</span> ");
            div.push("<span class='nn'>"+nn+"</span>");
            div.push("</li>");

            ssC++;
            matC = matC + parseInt(nn);

        } );

        div.push("</ul>");
        div.push("</div>");

        div.push("</div>");

        div = div.join( "\n" );
        div = div.replace("{matC}",matC);
        div = div.replace("{ssC}",ssC);

        this.html.push(div);
    }

    $("div.byMat").html( this.html.join("\n") );
}

$(document).ready(function () {
    //$("a.default").click();
    $("tr.event td").on("click", function () {
        console.log("::", this);
    });
    //eventTableList()
});

$(window).ready(function () {
    $("tr.event td").on("click", function () {
        $("#finder input").val( $(this).text().toLowerCase() ).keyup();
        $(this).addClass("sel");
        console.log(this);
    });

    $("div.sys").on("click",function(){
        $(this).find("div.bySysList").toggle(450);
    });

    $("div#finder input").on("click",function(){
        $(this).val(" ");
        $(this).unbind("click");
    });
    

});