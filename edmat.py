from functions import *
import config, json, os, traceback, urllib.request

events = []

print("load data from: " + config.journal_path)
edj_files = os.listdir(config.journal_path)


def lines2data(path):
    ar = []
    try:
        fp = open(path, "r", encoding='utf-8')
        lines = fp.readlines()
        line_c = 0
        fp.close()
    except UnicodeDecodeError:
        print(path)
        fp.close()

    for line in lines:
        try:
            line = line.replace(":inf", ':"infinite"')
            asJson = json.loads(line)
            line_c = line_c + 1
            ar.append(asJson)
        except json.decoder.JSONDecodeError:
            print("ERR 23: " + line)

    return ar


nn = 0;
for nn in range(config.max):
    try:
        nn = nn + 1
        path2log = config.journal_path + "/" + edj_files[nn]
        jdatas = lines2data(path2log)
        for jdata in jdatas:
            events.append(jdata)
    except IndexError:
        break

currentSys = ""
currentEvent = ""
materials = []
C = 0
errC = 0
for e in events:
    try:
        if e['event'] == "FSDJump":
            currentSys = e['StarSystem']
            currentEvent = "FSDJump"
        elif e['event'] == "Location":
            currentSys = e['StarSystem']
            currentEvent = "Location"
        else:
            if e['event'] == "MaterialCollected":
                e['StarSystem'] = currentSys
                e['eventType'] = currentEvent
                materials.append(e)
    except True:
        traceback.print_exc()
        errC = errC + 1

    C = C + 1

print(str(len(edj_files)) + " total files, max used: " + str(config.max))
print(str(C) + " Events read")
print(str(errC) + " Errors")
# print(json.dumps(materials))


def sys2matTable(ar):
    allSS = {}

    for sys in ar:
        allSS[sys['StarSystem']] = []

    for SS in allSS:
        for ev in ar:
            if ev["StarSystem"] == SS:
                for x in range(ev['Count']):
                    mtr = {'name': ev['Name'], "category": ev['Category'], "count": x + 1}
                    mtrC = ev['Name']
                    allSS[SS].append(mtrC)

    allSysMat = allSS;

    # f = open("mtr.json", "w")
    # f.write(json.dumps(allSS))

    gridData = []
    for SS in allSysMat:
        for ev in ar:
            mat = ev['Name']
            matC = allSysMat[SS].count(mat)
            tmp = SS + "|" + ev['Category'] + "|" + ev['Name'] + "|" + str(matC)
            if not matC == 0:
                gridData.append(tmp)

    #gridData = remove_duplicates(gridData)
    gridData = unique(gridData)

    reportBySys = {}
    for row in gridData:
        tmp = row.split("|")
        sys = tmp[0]
        reportBySys[sys] = []

    for row in gridData:
        tmp = row.split("|")
        sys = tmp[0]
        reportBySys[sys].append({"type": tmp[1], tmp[2]: str(tmp[3])})

    reportByMat = {}
    for row in gridData:
        tmp = row.split("|")
        mat = tmp[2]
        reportByMat[mat] = []

    for row in gridData:
        tmp = row.split("|")
        mat = tmp[2]
        sys = tmp[0]
        reportByMat[mat].append({sys: str(tmp[3])})

    out = {"byMat": reportByMat, "bySys": reportBySys}

    return out

reports = sys2matTable(materials)

# jsave('db/bySys.json',reports['bySys'],nl=0)
# jsave('db/byMat.json',reports['byMat'],nl=0)
# jsave('db/events.json',materials,nl=0)

saveAsJsObj('db/bySys.js',reports['bySys'],"bySys")
saveAsJsObj('db/byMat.js',reports['byMat'],"byMat")
saveAsJsObj('db/events.js',materials,"events")


# download list materials form github raw file
print('Beginning file download form:\n::\t'+config.materialsPath)
matP = urllib.request.urlopen(config.materialsPath)
matDT = matP.read()
matJS = json.loads(matDT)
jsave("materials.json",matJS)
saveAsJsObj("db/materials.js",matJS,"materials")



