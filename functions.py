def unique(values):
    output = []
    seen = set()
    for value in values:
        # If value has not been encountered yet,
        # ... add it to both list and set.
        if value not in seen:
            output.append(value)
            seen.add(value)
    return output


def jdump(input=False, nl=1):
    import json
    if nl == 0:
        print(json.dumps(input))
    else:
        print(json.dumps(input, indent=nl))


def jsave(name="uncknow.json", input=False, nl=1):
    import json
    f = open(name, "w")
    if nl == 0:
        f.write(json.dumps(input))
    else:
        f.write(json.dumps(input, indent=nl))

def saveAsJsObj(path="uncknow.json", input=False, name="uncknow"):
    import json
    f = open(path, "w")
    js = "var "+name+" = "+json.dumps(input)+";\n console.log('loaded: "+name+"')"
    f.write(js)

def eventTable(ar=False):
    head = ar[0]
    tr_head = []
    tr_head.append("<tr id='ed-mat-head'>\n")
    for lab in head:
        tr_head.append("\t<th headers='h_" + lab + "'>" + lab + "</th>\n")

    tr_head.append("<tr>\n")
    tr_head = ''.join(tr_head)

    tbl = []
    tbl.append("<table id='ed-mat-data'>\n")
    tbl.append(tr_head)

    # oddEven = ['odd', 'even']
    # oddEvenAlt = 0

    for tr in ar:

        tbl.append("<tr class=''>")
        for td in tr:
            if not td == "Name_Localised":
                tbl.append("<td class='" + td + "'>")
                tbl.append(str(tr[td]))
                tbl.append("</td>")

        tbl.append("</tr>")

    tbl.append("</table>")

    table = " ".join(tbl)

    f = open("events.html", "w")
    f.write(table)
